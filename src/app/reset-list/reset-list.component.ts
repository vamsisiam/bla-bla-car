import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-reset-list',
  templateUrl: './reset-list.component.html',
  styleUrls: ['./reset-list.component.css']
})
export class ResetListComponent implements OnInit {

  constructor(private service:LoginService) { }

  ngOnInit(): void {
  }
  Reset(data:NgForm){
    console.log(data.value)
    this.service.Password=data.value.password
    console.log(this.service.Password)
  }

}
