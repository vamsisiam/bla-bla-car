import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ResetListComponent } from './reset-list/reset-list.component';
import { UserListComponent } from './user-list/user-list.component';

const routes: Routes = [
  { path: '', pathMatch: "full", redirectTo: 'login' },
  { path: 'login', component: LoginComponent },
  { path: 'reset', component: ResetListComponent },
  {path:  'user',  component: UserListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
